<?php

/**
 * @file
 * Main file for Entity Config module.
 */

/**
 * Entity Config class.
 */
class EntityConfig extends stdClass {

  private $type;

  /**
   * Create EntityConfig object.
   *
   * @param string $type
   *   Name of entity.
   */
  public function __construct($type) {
    $this->type = $type;
  }

  /**
   * Overview EntityConfig objects.
   */
  public function overview() {
    $entity_info = entity_get_info($this->type);
    $output = $entity_info['entity config data']['output']['data'];
    $id_key = !empty($entity_info['entity keys']['id']) ? $entity_info['entity keys']['id'] : 'id';

    // Actions link.
    $actions = array(
      'header' => t('Actions'),
      'link' => l(t('edit'), $entity_info['entity config data']['admin path'] . "/manage/%id/edit")
      . ' ' . l(t('delete'), $entity_info['entity config data']['admin path'] . "/manage/%id/delete"),
    );
    if (isset($entity_info['entity config data']['output']['actions'])) {
      if (isset($entity_info['entity config data']['output']['actions']['header'])) {
        $actions['header'] = $entity_info['entity config data']['output']['actions']['header'];
      }
      if (isset($entity_info['entity config data']['output']['actions']['link'])) {
        $actions['link'] = $entity_info['entity config data']['output']['actions']['link'];
      }
    }
    $actions['link'] = rawurldecode($actions['link']);

    // Make content.
    $content = array();
    // Load all of our entities.
    $entities = entity_load($this->type);
    if (!empty($entities)) {
      $rows = $header = array();
      foreach ($entities as $entity) {
        $data = array();
        foreach ($output as $key => $header_item) {
          $data[$key] = filter_xss($entity->{$key});
        }
        $data[] = strtr($actions['link'], array('%id' => $entity->{$id_key}));
        $rows[] = $data;
      }
      foreach ($output as $key => $header_item) {
        $header[] = $header_item;
      }
      $header[] = $actions['header'];

      $content['entity_table'] = array(
        '#theme' => 'table',
        '#rows' => $rows,
        '#header' => $header,
      );
    }
    else {
      // There were no entities. Tell the user.
      $content[] = array(
        '#type' => 'item',
        '#markup' => t('No config entities currently exist.'),
      );
    }

    return $content;
  }

  /**
   * Add new object.
   */
  public function add() {
    $entity = entity_get_controller($this->type)->create();
    return drupal_get_form('entity_config_edit_form', $entity);
  }

  /**
   * Edit object.
   *
   * @param int $id
   *   Object ID.
   */
  public function edit($id) {
    $entity = reset(entity_load($this->type, array($id)));
    $entity->type = $this->type;
    return drupal_get_form('entity_config_edit_form', $entity);
  }

  /**
   * Delete object.
   *
   * @param int $id
   *   Object ID.
   */
  public function delete($id) {
    $entity = reset(entity_load($this->type, array($id)));
    $entity->type = $this->type;
    return drupal_get_form('entity_config_delete_form', $entity);
  }

}

/**
 * EntityConfigControllerInterface definition.
 *
 * We create an interface here because anyone could come along and
 * use hook_entity_info_alter() to change our controller class.
 * We want to let them know what methods our class needs in order
 * to function with the rest of the module, so here's a handy list.
 */
interface EntityConfigControllerInterface extends DrupalEntityControllerInterface {

  /**
   * Create an entity.
   */
  public function create();

  /**
   * Save an entity.
   *
   * @param object $entity
   *   The entity to save.
   */
  public function save($entity);

  /**
   * Delete an entity.
   *
   * @param object $entity
   *   The entity to delete.
   */
  public function delete($entity);

}

/**
 * Subclass of DrupalDefaultEntityController.
 */
class EntityConfigController extends DrupalDefaultEntityController implements EntityConfigControllerInterface {

  /**
   * Create and return a new config entity.
   */
  public function create() {
    $entity = new stdClass();
    $entity->type = $this->entityType;

    $entity_info = entity_get_info($entity->type);
    $id_key = !empty($entity_info['entity keys']['id']) ? $entity_info['entity keys']['id'] : 'id';
    $entity->id_key = $id_key;
    $entity->{$id_key} = 0;

    return $entity;
  }

  /**
   * Saves config entities using drupal_write_record().
   */
  public function save($entity) {
    module_invoke_all('entity_presave', $entity, $entity->type);

    // Write out the entity record.
    $entity_info = entity_get_info($entity->type);
    $id_key = !empty($entity_info['entity keys']['id']) ? $entity_info['entity keys']['id'] : 'id';
    $primary_keys = $entity->{$id_key} ? $id_key : array();
    drupal_write_record($entity_info['base table'], $entity, $primary_keys);

    // Operation invoke.
    $invocation = 'entity_insert';
    if (empty($primary_keys)) {
      field_attach_insert($entity->type, $entity);
    }
    else {
      field_attach_update($entity->type, $entity);
      $invocation = 'entity_update';
    }
    module_invoke_all($invocation, $entity, $entity->type);

    return $entity;
  }

  /**
   * Delete a single entity.
   */
  public function delete($entity) {
    $entity_info = entity_get_info($entity->type);
    $id_key = !empty($entity_info['entity keys']['id']) ? $entity_info['entity keys']['id'] : 'id';

    db_delete($entity_info['base table'])
      ->condition($id_key, $entity->{$id_key}, '=')
      ->execute();
  }

}

/**
 * Add and edit form.
 */
function entity_config_edit_form($form, &$form_state, $entity) {

  // Import input for form.
  $entity_info = entity_get_info($entity->type);
  $input = $entity_info['entity config data']['input']['data'];
  foreach ($input as $field_name => $field_data) {
    // Options.
    if (isset($field_data['#options']) && is_string($field_data['#options'])) {
      $options_function = $field_data['#options'];
      if (function_exists($options_function)) {
        $input[$field_name]['#options'] = call_user_func($options_function);
      }
    }

    // Default values.
    if (isset($entity->{$field_name})) {
      $input[$field_name]['#default_value'] = $entity->{$field_name};
    }
  }

  $form['entity_config'] = array($input);

  $form['entity_config_object'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  field_attach_form($entity->type, $entity, $form, $form_state);

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => $entity_info['entity config data']['admin path'],
  );

  // Custom validate function.
  if (!empty($entity_info['entity config data']['input']['validate'])) {
    $form['#validate'][] = $entity_info['entity config data']['input']['validate'];
  }

  return $form;
}

/**
 * Form submission for entity_config_edit_form().
 */
function entity_config_edit_form_submit($form, &$form_state) {
  $entity = entity_config_ui_form_submit_build_entity($form, $form_state);
  entity_get_controller($entity->type)->save($entity);
  drupal_set_message(t('New item has been created'));
  $form_state['redirect'] = $entity->admin_path;
}

/**
 * Delete form.
 */
function entity_config_delete_form($form, &$form_state, $entity) {
  $form['entity_config_object'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  $entity_info = entity_get_info($entity->type);
  $id_key = !empty($entity_info['entity keys']['id']) ? $entity_info['entity keys']['id'] : 'id';

  return confirm_form($form,
    t('Are you sure you want to delete item ID=%id?', array('%id' => $entity->{$id_key})),
    $entity_info['entity config data']['admin path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Form submission for entity_config_delete_form().
 */
function entity_config_delete_form_submit($form, &$form_state) {
  $entity = entity_config_ui_form_submit_build_entity($form, $form_state);
  entity_get_controller($entity->type)->delete($entity);
  drupal_set_message(t('The item has been deleted'));
  $form_state['redirect'] = $entity->admin_path;
}

/**
 * Fill entity properties form values and entity return.
 */
function entity_config_ui_form_submit_build_entity($form, $form_state) {
  $entity = $form_state['values']['entity_config_object'];

  if (isset($form['entity_config'])) {
    foreach ($form['entity_config'][0] as $key => $value) {
      if (isset($form_state['values'][$key])) {
        $entity->{$key} = filter_xss($form_state['values'][$key]);
      }
    }

    field_attach_submit($entity->type, $entity, $form, $form_state);
  }

  $entity_info = entity_get_info($entity->type);
  $entity->admin_path = $entity_info['entity config data']['admin path'];

  return $entity;
}
